let listUser = [];
const BASE_USER = 'https://63c14f3b99c0a15d28e697ff.mockapi.io';

function fetchUserList() {
    batLoading();
    axios({
        url: `${BASE_USER}/user`,
        method: 'GET',
    })
    .then(function(res) {
        tatLoading();
        listUser = res.data;
        renderUser(res.data);
    })
    .catch(function(err) {
        
    })
}
//chạy lần đầu khi load trang
fetchUserList();

function gan() {
    console.log("🚀 ~ file: services.js:2 ~ listUser", listUser)
}
function themUser() {

    var user = layThongTinForm();
        console.log("🚀 ~ file: services.js:28 ~ themUser ~ user", user)
        var isValid = required(user.taiKhoan, 'tbTaiKhoan') && kiemTraTrung(user.taiKhoan,listUser);
        isValid &= validateForm(user);
        ////
        if(isValid) {
            console.log('okok')
            batLoading();
            axios({
                url: `${BASE_USER}/user`,
                method: 'POST',
                data: user,
            })
            .then(function(res) {
                tatLoading();
                clearForm();
                fetchUserList();
            })
            .catch(function(err) {
                tatLoading();
            })
            document.getElementById('themUser').setAttribute('data-dismiss', 'modal');
        }

}

function xoaUser(id) {
    batLoading();
    axios({
        url: `${BASE_USER}/user/${id}`,
        method: 'DELETE',
    })
    .then(function(res) {
        tatLoading();
        fetchUserList();
    })
    .catch(function(err) {
        tatLoading();
    })
}

// UPDATE phần 1: Lấy thông tin show lên form
function suaThongTin(id) {
    batLoading();
    axios({
        url: `${BASE_USER}/user/${id}`,
        method: 'GET'
    })
    .then(function(res) {
        tatLoading();
        document.getElementById('TaiKhoan').disabled = true;
        document.getElementById('capNhatUser').removeAttribute('disabled');
        document.getElementById('themUser').setAttribute('disabled', 'true');
        document.getElementById('capNhatUser').removeAttribute('data-dismiss', 'modal');

        clearThongBao();
        showThongTinForm(res.data);
    })
    .catch(function(err) {
        tatLoading();
    })
}

// UPDATE phần 2: Cho sửa thông tin trên form => Nhấn nút Cập nhật thì lưu thay đổi
function capNhatThongTin() {
    var newData = layThongTinForm();
    var isValid = validateForm(newData);
    if(isValid) {
        batLoading();
        axios({
            url: `${BASE_USER}/user/${newData.id}`,
            method: 'PUT',
            data: newData,
        })
        .then(function(res) {
            tatLoading();
            clearForm();
            fetchUserList();
        })
        .catch(function(err) {
            tatLoading();
        });
        document.getElementById('TaiKhoan').disabled = false;
        document.getElementById('capNhatUser').setAttribute('data-dismiss', 'modal');
    }
}

