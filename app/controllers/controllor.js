
// bật loading
function batLoading() {
    document.getElementById('loading').style.display = 'flex';
}

// tắt loading
function tatLoading() {
    document.getElementById('loading').style.display = 'none';
}

function renderUser(userARR) {
    var contentHTML = '';
    userARR.forEach(function(user) {
        
        var contentTr = `<tr>
                            <td>${user.id}</td>
                            <td>${user.taiKhoan}</td>
                            <td>${user.matKhau}</td>
                            <td>${user.hoTen}</td>
                            <td>${user.email}</td>
                            <td>${user.ngonNgu}</td>
                            <td>${user.loaiND}</td>
                            <td>
                                <button onclick='xoaUser(${user.id})' class='btn btn-danger'>Xoá</button>
                                <button onclick='suaThongTin(${user.id})' class='btn btn-warning' data-toggle="modal" data-target="#myModal">Sửa</button>
                            </td>
                        </tr>` 
                            //<img src="../../assets/img/${user.hinhAnh}" alt="">
        contentHTML += contentTr;
    });
    document.getElementById('tblDanhSachNguoiDung').innerHTML = contentHTML;
}

// lấy thông tin từ form
function layThongTinForm() {
    var idUser = document.getElementById('idUser').value;
    var taiKhoan = document.getElementById('TaiKhoan').value;
    var hoTen = document.getElementById('HoTen').value;
    var matKhau = document.getElementById('MatKhau').value;
    var email = document.getElementById('Email').value;
    var hinhAnh = document.getElementById('HinhAnh').value;
    var loaiND = document.getElementById('loaiNguoiDung').value;
    var ngonNgu = document.getElementById('loaiNgonNgu').value;
    var moTa = document.getElementById('MoTa').value;

    // var user = {
    //     id: idUser,
    //     taiKhoan: taiKhoan,
    //     hoTen: hoTen,
    //     matKhau: matKhau,
    //     email: email,
    //     hinhAnh: hinhAnh,
    //     loaiND: loaiND,
    //     ngonNgu: ngonNgu,
    //     moTa: moTa,
    // }
    var user = new Teacher (
        taiKhoan,
        hoTen,
        matKhau,
        email,
        hinhAnh,
        loaiND,
        ngonNgu,
        moTa
    )
    return user;
}

//show thông tin lênn form
function showThongTinForm(user) {
    document.getElementById('idUser').value = user.id;
    document.getElementById('TaiKhoan').value = user.taiKhoan;
    document.getElementById('HoTen').value = user.hoTen;
    document.getElementById('MatKhau').value = user.matKhau;
    document.getElementById('Email').value = user.email;
    document.getElementById('HinhAnh').value = user.hinhAnh;
    document.getElementById('loaiNguoiDung').value = user.loaiND;
    document.getElementById('loaiNgonNgu').value = user.ngonNgu;
    document.getElementById('MoTa').value = user.moTa;
}

//clear form
function clearForm() {
    document.getElementById('TaiKhoan').value = '';
    document.getElementById('HoTen').value = '';
    document.getElementById('MatKhau').value = '';
    document.getElementById('Email').value = '';
    document.getElementById('HinhAnh').value = '';
    document.getElementById('loaiNguoiDung').selectedIndex = 0;
    document.getElementById('loaiNgonNgu').selectedIndex = 0;
    document.getElementById('MoTa').value = '';
}
function clearThongBao() {
    document.getElementById('tbTaiKhoan').innerText = '';
    document.getElementById('tbHoTen').innerText = '';
    document.getElementById('tbMatKhau').innerText = '';
    document.getElementById('tbEmail').innerText = '';
    document.getElementById('tbHinhAnh').innerText = '';
    document.getElementById('tbloaiNguoiDung').innerText = '';
    document.getElementById('tbloaiNgonNgu').innerText = '';
    document.getElementById('tbMoTa').innerText = '';
}
// kiểm tra form khi nhấn nút thêm mới
function ktForm(){
    document.getElementById('themUser').removeAttribute('disabled');
    document.getElementById('capNhatUser').setAttribute('disabled', 'true');
    document.getElementById('themUser').removeAttribute('data-dismiss', 'modal');
    // nếu người dùng ko cập nhật nữa và bấm nút thoát thì kiểm tra lại form khi nhấn nút thêm mới+
    var temp = document.getElementById('TaiKhoan').disabled;
    if(temp) {
        document.getElementById('TaiKhoan').disabled = false;
        clearForm();
        clearThongBao();
    }
}
document.getElementById('btnThemNguoiDung').addEventListener('click', function() {
    ktForm();
})
