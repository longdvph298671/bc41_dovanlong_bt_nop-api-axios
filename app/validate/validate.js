
function kiemTraTrung(value, listData) {
    var index = listData.findIndex(function (item) {
        return item.taiKhoan == value;
    })
    
    if(index != -1) {
        document.getElementById('tbTaiKhoan').style.display = 'block';
        document.getElementById('tbTaiKhoan').innerText = '*Tài khoản đã tồn tại!';
        return false;
    }
    else {
        document.getElementById('tbTaiKhoan').innerText = '';
        return true;
    }
}
// required: không được để trống
function required(value, idErr) {
    var length = value.length;
    if(length === 0) {
        document.getElementById(idErr).style.display = 'block';
        document.getElementById(idErr).innerText = '*Trường này bắt buộc nhập!'
        return false;
    }
    else {
        document.getElementById(idErr).innerText = '';
        return true;
    }
}
// không chứa sô và kí tự đặc biệt
function string(value, idErr) {
    let pattern = /^(([a-zA-Z\sÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ]*)([a-zA-Z\s\'ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ]*)([a-zA-Z\sÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ]))*$/g;
    if(!pattern.test(value)) {
        document.getElementById(idErr).style.display = 'block';
        document.getElementById(idErr).innerText = '*Họ tên không chứa số và ký tự đặc biệt!';
        return false;
    }
    else {
        document.getElementById(idErr).innerText = '';
        return true;
    }
}

// check mật khẩu 
function checkPassord(value, idErr) {
    let pattern = /^(?=.*?[A-Z]{1,})(?=(.*[\d]){1,})(?=(.*[\W]){1,}).{6,10}$/;
    if(!pattern.test(value)) {
        document.getElementById(idErr).style.display = 'block';
        document.getElementById(idErr).innerText = '*Mật khẩu chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt!';
        return false;
    }
    else {
        document.getElementById(idErr).innerText = '';
        return true;
    }
}

// kiểm tra độ dài
function kiemTraDoDai(value, idErr, min, max) {
    var length = value.length;
    if(length > max || length < min) {
        document.getElementById(idErr).style.display = 'block';
        document.getElementById(idErr).innerHTML = `*Độ dài phải từ ${min} đến ${max} ký tự!`;
        return false;
    }
    else {
        document.getElementById(idErr).innerText = '';
        return true;
    }
}

// check email
function checkEmail(value, idErr) {
    let pattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if(!pattern.test(value)) {
        document.getElementById(idErr).style.display = 'block';
        document.getElementById(idErr).innerText = '*Email phải đặt theo cú pháp text@gmail.com!';
        return false;
    }
    else {
        document.getElementById(idErr).innerText = '';
        return true;
    }
}

//check select
function checkSelect(idSelect, idErr, textSe) {
    var selectedIndex = document.getElementById(idSelect).selectedIndex;
    if(selectedIndex === 0) {
        document.getElementById(idErr).style.display = 'block';
        document.getElementById(idErr).innerText = textSe;
        return false;
    }
    else {
        document.getElementById(idErr).innerText = '';
        return true;
    }
}

// kiểm tra độ dài mô tả
function kiemTraDoDaiMoTa(value, idErr, max) {
    var length = value.length;
    if(length > max) {
        document.getElementById(idErr).style.display = 'block';
        document.getElementById(idErr).innerHTML = `*Độ dài không quá ${max} ký tự!`;
        return false;
    }
    else {
        document.getElementById(idErr).innerText = '';
        return true;
    }
}


function validateForm(newUser) {
    var isValid = true;
    isValid = required(newUser.hoTen, "tbHoTen") && string(newUser.hoTen, 'tbHoTen');
    isValid &= required(newUser.matKhau, 'tbMatKhau') 
                && checkPassord(newUser.matKhau, 'tbMatKhau')
                && kiemTraDoDai(newUser.matKhau, 'tbMatKhau', 6, 8);
    isValid &= required(newUser.email, 'tbEmail') && checkEmail(newUser.email, 'tbEmail');
    isValid &= required(newUser.hinhAnh, 'tbHinhAnh');
    isValid &= checkSelect('loaiNguoiDung', 'tbloaiNguoiDung', '*Chưa chọn loại người dùng!');
    isValid &= checkSelect('loaiNgonNgu', 'tbloaiNgonNgu', '*Chưa chọn ngôn ngữ!');
    isValid &= required(newUser.moTa, 'tbMoTa') && kiemTraDoDaiMoTa(newUser.moTa, 'tbMoTa', 60);
    return isValid;
}