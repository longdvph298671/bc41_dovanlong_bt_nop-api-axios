
function headerScroll() {
    var header = document.getElementById('header');
    header.classList.toggle('sticky', window.scrollY > 250);
}
window.addEventListener('scroll', function() {
    headerScroll();
})