
batLoading();
axios ({
    url: 'https://63c14f3b99c0a15d28e697ff.mockapi.io/user',
    method: "GET",
})
.then(function (res) {
    tatLoading();
    renderTeacher(res.data);
})
.catch(function (err) {
    tatLoading();
})

function renderTeacher(listTeacher) {
    var contentHTML = '';
    listTeacher.forEach(function(item) {
        if(item.loaiND === 'GV') {
            var contentItem = `<div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
                                    <div class="teacher-item">
                                        <div class="teacher-img">
                                            <img src="./img/${item.hinhAnh}" alt="">
                                        </div>
                                        <div class="teacher_content">
                                            <h5>${item.ngonNgu}</h5>
                                            <h3>${item.hoTen}</h3>
                                            <p>${item.moTa}</p>
                                        </div>
                                    </div>
                                </div>`
            contentHTML += contentItem;
        }
    });
    document.getElementById('listTeacher').innerHTML = contentHTML;
}

function batLoading() {
    var loaDing = document.getElementById('loaDing');
    loaDing.style.display = 'flex';
}
function tatLoading() {
    var loaDing = document.getElementById('loaDing');
    loaDing.style.display = 'none';
}